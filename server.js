var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var firebase = require("firebase");

firebase.initializeApp({
    databaseURL: "https://calc-app-831b8.firebaseio.com",
    serviceAccount: "./calc-app-a8efa86694e3.json",
});

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.post('/users', function (req, res) {
    var user = req.body;
    var db = firebase.database();
    var dbRefUsers = db.ref('/users');
    dbRefUsers.once('value', function (snapshot) {
        var users = snapshot.val();
        var arr = Object.keys(users).map(function (key) { return users[key] });
        var bFoundUserEmail = false;
        arr.map(function (item) {
            if (item.email === user.email) {
                bFoundUserEmail = true;
            }
        });

        console.log(bFoundUserEmail);
        if (bFoundUserEmail) {
            //return error
            res.json({ message: 'Email already registered!', error: true });
        }
        else {
            //add user and return success
            dbRefUsers.push(user);
            res.send(JSON.stringify({ message: 'Email successfully registered!', error: false }));
        }
    }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
    });
});

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.listen(3000, function () {
    console.log('Server listening on port 3000!');
});